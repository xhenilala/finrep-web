<?php 

namespace App;

use WP_Query;

class AjaxCall
{

	/**
	*  __construct
	*/
	function __construct(){ 

		global $wpdb;
		$this->wpdb = $wpdb;

		$actions = [
			//'action_name_here',
			
		];

		foreach( $actions as $action ){
			add_action( 'wp_ajax_'         .$action   , [ $this, $action ] );
			add_action( 'wp_ajax_nopriv_'  .$action   , [ $this, $action ] );
		}
	}


} // class

return new \App\AjaxCall();