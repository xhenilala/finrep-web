<?php

namespace App;

class Debrand
{

	public function __construct(){


		show_admin_bar(false);

		
		if( !current_user_can('administrator')){
			add_action( 'admin_menu'        , array( $this, 'remove_menus') );
		}

		
		add_action( 'admin_menu'		    , array( $this, 'adjust_the_wp_menu' ), 999 );
		add_action( 'wp_dashboard_setup'    , array( $this, 'remove_all_dashboard_meta_boxes'), 9999 );
		add_action( 'admin_head-index.php'  , array( $this, 'dashboard_scripts') );
		add_action( 'login_enqueue_scripts' , array( $this, 'login_scripts' ) );
		add_action( 'admin_footer_text'		, array( $this, 'credits_newmedia' ) );
		add_action( 'admin_bar_menu'		, array( $this, 'remove_wp_logo' ), 999 );
		add_filter('wp_headers'				, array( $this, 'wp_headers'));
		add_action('wp'						, array( $this, 'rsd_link'), 11);
		add_action('init'					, array( $this,'disable_embeds_init' ), 9999);
		add_filter( 'nav_menu_item_id'		, array( $this, 'filter_menu_id') );
		
		remove_action( 'wp_head'			, 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head'			, 'wp_oembed_add_discovery_links', 10 );		
		remove_action( 'template_redirect'	, 'rest_output_link_header', 11, 0 );
		add_action('after_setup_theme',array($this,'remove_core_updates'));
		$this->header_cleanup();
		add_action('wp_before_admin_bar_render', array($this, 'no_wp_logo_admin_bar_remove' ), 0 );
	}


	public function filter_menu_li(){
	    return array('');   
	}

	public function filter_menu_id(){
	    return; 
	}

	public function disable_embeds_init(){
	    remove_action('rest_api_init', 'wp_oembed_register_route');
	    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
	    remove_action('wp_head', 'wp_oembed_add_discovery_links');
	    remove_action('wp_head', 'wp_oembed_add_host_js');
	}

	public function rsd_link(){
		remove_action('wp_head', 'rsd_link');
	}

	public function wp_headers( $headers ){
		unset($headers['X-Pingback']);
		return $headers;
	}

	public function header_cleanup(){
		remove_action('wp_head', 'rsd_link');
	    remove_action('wp_head', 'wp_generator');
	    remove_action('wp_head', 'feed_links', 2);
	    remove_action('wp_head', 'feed_links_extra', 3);
	    remove_action('wp_head', 'index_rel_link');
	    remove_action('wp_head', 'wlwmanifest_link');
	    remove_action('wp_head', 'start_post_rel_link', 10, 0);
	    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	    remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	    remove_action('admin_print_styles', 'print_emoji_styles');
	    remove_action('wp_head', 'print_emoji_detection_script', 7);
	    remove_action('admin_print_scripts', 'print_emoji_detection_script');
	    remove_action('wp_print_styles', 'print_emoji_styles');
	    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	    remove_filter('the_content_feed', 'wp_staticize_emoji');
	    remove_filter('comment_text_rss', 'wp_staticize_emoji');
	}
	
	public function remove_menus(){
		remove_menu_page( 'index.php' );
		remove_menu_page( 'upload.php' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'tools.php' );
		remove_submenu_page( 'options-general.php', 'options-writing.php' );
		remove_submenu_page( 'options-general.php', 'options-media.php' );
		remove_submenu_page( 'options-general.php', 'options-discussion.php' );
		remove_submenu_page( 'options-general.php', 'options-reading.php' );
		remove_submenu_page( 'users.php', 'profile.php' );
		remove_menu_page( 'users.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page('wpseo_dashboard');
		remove_menu_page( 'visual-form-builder' );
		remove_menu_page( 'wpfastestcacheoptions' );
		remove_menu_page( 'options' );
		remove_menu_page( 'edit.php?post_type=acf-field-group' );
	}

	public function adjust_the_wp_menu() {
		$page = remove_submenu_page( 'index.php','update-core.php');
	}

	public function remove_all_dashboard_meta_boxes(){
	    global $wp_meta_boxes;
	    $wp_meta_boxes['dashboard']['normal']['core'] = array();
	    $wp_meta_boxes['dashboard']['side']['core'] = array();
	}

	function remove_wp_logo( $wp_admin_bar ) {
		$wp_admin_bar->remove_node( 'wp-logo' );
		$wp_admin_bar->remove_node( 'updates' );
		$wp_admin_bar->remove_node( 'comments' );
		//$wp_admin_bar->remove_node( 'new-content' );
		$wp_admin_bar->remove_node( 'wpseo-menu' );
		$wp_admin_bar->remove_node( 'customize' );
		$wp_admin_bar->remove_node( 'themes' );
		$wp_admin_bar->remove_node( 'menus' );
		$wp_admin_bar->remove_node( 'widgets' );
		$wp_admin_bar->remove_node( 'new-media' );
		$wp_admin_bar->remove_node( 'new-page' );
		$wp_admin_bar->remove_node( 'new-user' );
	}

	public function remove_core_updates(){
	 	if(! current_user_can('update_core')){return;}
	 	add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
	 	add_filter('pre_option_update_core','__return_null');
		 add_filter('pre_site_transient_update_core','__return_null');
	}


	function credits_newmedia () {
  		print 'Developed by <a href="#" target="_blank">New Media Communications</a>';
  	}

  	public function login_scripts() { ?>

	    <style type="text/css">
		       	/*body.login{
		       		height: 100vh;
		       		min-height: 600px;
		       		background-color: #12549f;
		       		background-image: url(<?php echo get_template_directory_uri() . '/images/admin-bg.jpg' ?>);
		       		background-repeat: no-repeat;
		       		background-position: center center;
		       		background-size: cover;
		       	}

		       	#login h1{
		       		display: none;
		       		float: left;
		       		width: 100%;
		       		background-color: #fff;
		       	}

		        #login h1 a, 
	    		.login h1 a {
		   			float: left;
		   			width: 100%;
		   			background-repeat:no-repeat;
		   			background-size:contain;
		            padding-bottom: 0;
		            margin-bottom:0;
		            background-image: url(<?=get_stylesheet_directory_uri() . '/images/undefined.png';?>);
		       		outline:0;
		       		border:none;
		        }

		        .login #login_error, .login .message{
		        	background: transparent!important;
		        	color: #fff!important
		        }


		        body.login #login h1 a{
					background-image: url(<?php echo get_template_directory_uri(); ?>/images/logo.png);
					background-size: contain;
					width:60%;
					margin:0 auto;
					margin-left: 20%;
					margin-right: 20%;
					outline: 0;
					background-color: #fff;
				}

				body.login #login h1 a:active, body.login #login h1 a:hover, body.login #login h1 a:focus{
					outline:0;
					box-shadow: none;
				}

		       	.login form{
		       		float:left;
		       		margin-top:10px;

		       		background-color: rgba(0,0,0,.5) !important;
		       	}
		       	.login #backtoblog, .login #nav{
		       		display: none;
		       	}*/
		</style>
	<?php }


	public function dashboard_scripts() { ?>
	        <style>#icon-index, .wrap h2 {display:none} </style>
	        <script language="javascript" type="text/javascript">
	            jQuery(document).ready(function($) {
	               $('#dashboard-widgets-wrap').remove();
	               $('#vfb-sidebar').remove();
	               $('#login h1 a, .login h1 a').click(function(e){
	               		e.preventDefault();
	               });
	            });
	        </script>
	<?php }

	function no_wp_logo_admin_bar_remove(){ ?>
        <style type="text/css">
            .index-php .wrap{
				display: none;
            }
            .index-php #screen-meta-links{
            	display: none;
            }
            #footer-upgrade{
            	display: none;
            }         
            #vfb-sidebar{
            	display: none !important;
            }
            #vfb-form-list{
            	margin-left: 0;
            }

        </style>
        <script language="javascript">
        	jQuery(document).ready(function($){
        		//$('#adminmenu').after('<div id="newmedia-logo"></div>');
        		$('#wpadminbar .quicklinks').before('<div id="nmc-logo"></div>');
        	});
        </script><?php
	}


}// Class

$debrand = new Debrand();