<?php 

namespace App;

use \WP_Query;

class Query
{

	function __construct()
	{
		# code...
	}

	static public function slideshow(){
		$args = [
			'post_type' => 'slideshow',
			'posts_per_page' => -1,
		];

		return new \WP_Query( $args );
	}


	static public function parties(){
		$args = [
			'post_type' => 'parties',
			'posts_per_page' => -1,
		];

		return new \WP_Query( $args );
	}


	static function legal(){
		$args = [
			'post_type' => 'legal',
			'post_per_page' => -1,
			'orderby' => 'date',
			'order' => 'ASC'

		];
		return new WP_Query( $args );
	}

	static function project_activities(){
		$args = [
			'post_type' => 'post',
			'post_per_page' => 3,
		];
		return new WP_Query( $args );
	}

	static function event(){
		$args = [
			'post_type' => 'event',
			'post_per_page' => '-1'
		];
		return new WP_Query( $args );
	}

	static function get_events_videos($per_page = 10){

		$args = [
			'post_type' => 'event',
			'post_per_page' => $per_page,
			'meta_query' => array(
				array(
					'key'     => 'event_videos',
					'value'   => '0',
					'compare' => '>=',
				),
			)
		];
		$query = new WP_Query( $args );
		
		$events = [];
		$i = 0;

		while( $query->have_posts() ) { 
			$query->the_post();
			$videos = get_field('event_videos');
			if(have_rows('event_videos')){
				 while(have_rows('event_videos')){
				  the_row();
				    $video_url = get_sub_field('event_video_url', false, false );
					parse_str( parse_url( $video_url, PHP_URL_QUERY ), $data );
					$video_id =  $data['v'];    

					if( $video_url ){
						$events[$i]['post_title'] = get_the_title();
						$events[$i]['post_content'] = get_the_content();
						$events[$i]['video_id'] = $video_id;
						$events[$i]['event_date'] = get_field('event_date');
						$events[$i]['thumbnail']  = \App\Utils::get_thumbnail_url('thumbnail');
					}
					$i++;
				}
			}
		}

		return $events;

	}
	
}// class 
