<?php 

namespace App;

class Theme
{

	public $version     = '1.0.0';
	public $text_domain = 'finance';

	const BASE_POST		= '/post-types/';
	const BASE_TAX 		= '/taxonomies/';
	const GMAP_API_KEY = 'AIzaSyD3LflChSNa9UM_LOt6-nkYNDH6yJZADPY';

	/**
	*  __construct
	*/
	function __construct(){

		$this->tpath = get_template_directory_uri();
		
		add_action( 'after_setup_theme'  , [ $this, 'setup' ] );
		
	}

	/**
	*  setup
	*/
	public function setup(){
		
		load_theme_textdomain( $this->text_domain, $this->tpath . '/languages' );

		add_action( 'init'				 		, [ $this , 'support'				] );
		add_action( 'init'				 		, [ $this , 'nav_menus'			    ] );
		add_action( 'init'						, [ $this , 'register_post_types'	] );
		add_action( 'init'			     		, [ $this , 'register_option_pages'	] );
		
		add_action( 'wp_enqueue_scripts' 		, [ $this , 'register_scripts' 		] );
		add_action( 'widgets_init'				, [ $this , 'sidebar'     			] );

		add_filter( 'excerpt_length', [$this , 'excerpt_length'  ], 999);

		add_filter('upload_mimes', [$this, 'cc_mime_types']); //import svg icons

		add_action( 'widgets_init'				, [ $this , 'register_strings'     		] ); # register_strings	

	}

	/**
	*  support
	*/
	public function support(){

		//title tag
		add_theme_support( 'title-tag' );

		//post thumbnails
		add_theme_support( 'post-thumbnails' );
		//set_post_thumbnail_size( 300, 300 );

		//post formats
		add_theme_support( 'post-formats', [ 'gallery', 'video' ] );
		//header
		add_theme_support( 'custom-header' );

		//html5
		add_theme_support( 'html5' , [ 
				'search-form'  , 
				'comment-form' , 
				'comment-list' , 
				'gallery'      , 
				'caption' 
			] );
		}

	/**
	*  nav_menus
	*  Register nav menus
	*/
	public function nav_menus(){
		register_nav_menus( 
			[
				'primary_en'   => esc_html__( 'Primary' , $this->text_domain ),
				'primary_sq'   => esc_html__( 'Primary' , $this->text_domain ),
				'footer_menu'   => esc_html__( 'Footer menu' , $this->text_domain ),
			]
		 );
	}

	/**
	*  register_post_types
	*/
	public function register_post_types(){

		if( !is_dir( TEMPLATEPATH . self::BASE_POST  ) || !is_dir( TEMPLATEPATH . self::BASE_TAX ) )
			return;

		$ext = '.php';

		// post types
		$post_types = [
			// self::BASE_POST . 'undefined',
			self::BASE_POST . 'slideshow',	
			self::BASE_POST . 'parties',
			self::BASE_POST . 'legal',
			self::BASE_POST . 'event',		
		];

		// taxonomies
		$taxonomies = [
			//self::BASE_TAX . 'undefined',
		];

		$files = array_merge( $post_types, $taxonomies );
		
		if( $files )
			foreach( $files as $file )
				if(	file_exists( TEMPLATEPATH . $file .$ext ) )
					include TEMPLATEPATH . $file .$ext;
	}

	/**
	*  register_option_pages
	*  register option pages - ACF Plugin is required
	*/
	public function register_option_pages(){
		
		if( !function_exists( 'acf_add_options_page' ) )
			return;

		// option page
		acf_add_options_page( [
				'page_title' => 'Options',
				'menu_title' => 'Options',
				'menu_slug'  => 'options',
				'capability' => 'edit_posts',
				'redirect'	 => false,
				'icon_url'   => 'dashicons-admin-generic',
		] );

		// option page homepage
		acf_add_options_page( [
				'page_title' => 'Homepage',
				'menu_title' => 'Homepage',
				'menu_slug'  => 'homepage',
				'capability' => 'edit_posts',
				'redirect'	 => false,
				'icon_url'   => 'dashicons-admin-home',
		] );
	}

	/**
	* register_scripts
	* register styles and scripts [ wp_head, wp_footer ] 
	*/
	public function register_scripts(){

		// css
		wp_enqueue_style( $this->text_domain . '_vendors' , $this->tpath . '/css/vendors.min.css' , [] , $this->version );
		wp_enqueue_style( $this->text_domain . '_site'    , $this->tpath . '/css/site.css' 		  , [] , $this->version );
		wp_enqueue_style( $this->text_domain . '_site_finrep'    , $this->tpath . '/css/site-finrep.css', [] , $this->version );

		// js
		if( !is_admin() ) wp_enqueue_script( 'jquery' );
		wp_enqueue_script( $this->text_domain . '_vendors' , $this->tpath . '/js/vendors.min.js' , [] , $this->version , 1 );
		wp_enqueue_script( $this->text_domain . '_site'    , $this->tpath . '/js/site.js' , [] , $this->version , 1 );

	}
	



	//gallery popup

	public function gallery_popup(){
		$gallery_id = (isset($_POST['gallery_id'])) ? (int)$_POST['gallery_id'] : '';
	    if ( $team_id ) 

	    	get_template_part( 'parts/gallery-popup' );

	    die;
	}


	/**
	* sidebar
	*/
	public function sidebar(){

		#sidebar
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', $this->text_domain ),
			'id'            => 'sidebar',
			'description'   => esc_html__( 'Sidebar', $this->text_domain ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
		) );

	}

	function excerpt_length( $length ) {
    return 15;
	}


	//import svg icons
	function cc_mime_types($mimes) {

	 $mimes['svg'] = 'image/svg+xml'; 
	 return $mimes; 
	}


	public function register_strings(){
		
		$strings = [
			'objectives','repairs','objective','No','Category','Description','apply now','See more','objective',
			'components','Downloads','prepares financial report','download','user financial report'	
		];

		if( $strings )
			foreach( $strings as $str )
				pll_register_string( $str, $str, 'finrep', false );

	}

}// class

return new \App\Theme();
