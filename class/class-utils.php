<?php

namespace App;

use \App\Query;

class Utils
{
	const API_KEY = 'AIzaSyAACg4GqO71yav45RvECgzUjSBs3F1WDaU';
	/**
	*  __construct
	*
	*/
	function __construct(){	

	}

	public static function tpath(){
		return get_template_directory_uri() . '/';
	}

	/**
	*  logo
	*/
	public static function logo( $key ){
		
		$arr =  [
			'desktop' =>  self::tpath() . 'images/logo.svg',
			'mobile'  =>  self::tpath() . 'images/logo.svg',
			'alt'     =>  get_bloginfo( 'name' )
		];
		
		return $arr[ $key ];
	}
	
	/**
	*  get_thumbnail_url
	*
	*  This function gets image url(featured image)
	*
	*  @param	$size    (string)
	*  @param   $post_id (int)
	*  @return	$image   (string)
	*/
	public static function get_thumbnail_url(  $size = 'full', $post_id = false  ){
		if ( $post_id == false ){
			global $post;
			$post_id = $post->ID;
		}
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ) , $size , true );
		if (is_array($image))
			return $image[0];
	}


		public static function getVideoThumbnail( $url ){
		
			parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
			$thumbnail = 'https://img.youtube.com/vi/'.$my_array_of_vars['v'].'/hqdefault.jpg' ;
			return $thumbnail;
		}
    

		static public function getVideoDescription($url){
			
	 		if(!$url)
	 			return;
	 		
	 		$v_data = [];

	 		parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
			$v_id = $my_array_of_vars['v'];
			
			$url = sprintf('https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=%s&key=%s', $v_id, self::API_KEY);
			
			$data = (new self)->readJson( $url );
			
			if( $data ){
				$v_data['title'] = $data[0]->snippet->title;
				$v_data['description'] = $data[0]->snippet->description;
			}

			return $v_data;
		}



		static public function readJson( $url ){
			
			$data = [];
			
			try {
				$json_data = file_get_contents( $url );
				$data = json_decode($json_data, false);
				if( is_object( $data ) && property_exists($data, 'items') )
					$data = $data->items;

			} catch (Exception $e) {
				var_dump($e);
			}
		
			return $data;	
		}




	/**
	*  contacts
	*
	*  @param   N/A
	*  @return  (object)
	*/
	public static function contacts(){
		if(!function_exists('get_field') )
			return;

		$contacts = [
			'location'  => get_field('location'   , 'options'),
			'email' 	=> get_field('email'	  , 'options'),
			'facebook'  => get_field('facebook'   , 'options'),
			'linkedin'   => get_field('linkedin'  , 'options'),
		];

		return $contacts;
	}



} // class
