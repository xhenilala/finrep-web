		</div><!--/.site-content-->
<?php $contacts = \App\Utils::contacts(); ?>
	<footer class="site-footer has-bg fw">
		<div class="container-fluid">
			<div class="footer-logo col-sm-2 col-xs-12">
				<img src="<?php echo \App\Utils::logo('desktop'); ?>" class="img-responsive img-logo" >
			</div>
			<div class="footer-menu col-sm-6 col-xs-12">
				<div class="cpr-container">
					<?php echo get_field('copyright_'.pll_current_language(),'options'); ?>
				</div>
			</div>
			<div class="footer-social col-sm-4 col-xs-12">
				<div class="socials">
					<?php get_template_part('parts/global/social') ?>
				</div>
			</div>
		</div>
	</footer>	

	<?php wp_footer(); ?>
	
	</body>
</html>