<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <link rel="profile"   href="http://gmpg.org/xfn/11">
    <link rel="pingback"  href="<?php bloginfo( 'pingback_url' ); ?>">  
        
    <!--favicon-->
    <link rel="icon" type="image/png" href="<?php echo \App\Utils::tpath() . 'images/favicon.png'?>" />

    <!--js scripts-->
    <script type="text/javascript">

        // admin ajax
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";

        // tpath
        var tpath = "<?php echo \App\Utils::tpath(); ?>";

         var map_marker = tpath + '/images/map-marker.png';
        
    </script>

    <!--wp head scripts-->
    <?php wp_head(); ?>

</head>
<body <?php body_class();?>>
<a href="#" class="scrollToTop" style="z-index: 9999999999;">
    <div>
        <img src="<?php echo get_template_directory_uri() . '/images/up.png'; ?>" alt="to-top" class="img-responsive top">
        <p><?php echo 'to top'; ?></p>
    </div>
</a>
<header id="header" class="site-header transition fw">
    <div class="container-fluid">
        <div class="row">
            <div class="logo-container col-md-2 col-6">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                    <img src="<?php echo \App\Utils::logo('desktop'); ?>" class="img-responsive img-logo" >
                </a>
            </div>
            <div class="menu-lang col-md-10">
                <nav class="navbar navbar-default " role="navigation">
                    <div class="navbar-header">
                        <button id="toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary_'.pll_current_language(),
                        'depth'             => 1,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'primary-menu',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker())
                    ); ?>
                </nav>
            </div>
        </div>
        
    </div>

    <div id="menu-container" class="menu-overlay col-xs-12 rmp" style="display: none;">
        <div class="container-fluid rmp">
        <div class="overlay-content has-bg col-xs-12 rmp">
            <div class="ov_menu col-sm-6 col-xs-12 rmp">
                 <?php wp_nav_menu( array(
                'theme_location'  => 'primary_'.pll_current_language(),
                ) ); ?>  
            </div>
        </div>
        </div>
    </div>

</header>

<div class="dots-bg">
    <div class="dots-container"></div>
</div>


    <div class="menu-btn rmp">
        <img src="<?php echo get_template_directory_uri() . '/images/menu-btn.png'; ?>" class="img-responsive">
    </div>
    <div class="mobile-menu hidden">
        <span class="close-menu"></span>
        <div class="final-menu-overlay">
            <div class="container">
                    <div class="row">
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'primary_'.pll_current_language(),
                                'menu_class'        => 'nav navbar-nav',
                            ) ); ?>
                    </div>
                    <div class="row">
                        
                        <div class="header-social-icons">
                            <div class="language-items">
                                <span>Language<?php pll_the_languages(array('show_flags'=> 0,'show_names'=> 1,'display_names_as'=> 'slug' )); ?></span>
                            </div>
                            <div class="social-icons">
                                <?php get_template_part( 'parts/global/social' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="site-content ">
