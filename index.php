<?php get_header();

	// slideshow
	echo '<section class="hm-section" id="section-0">';
	get_template_part('parts/homepage/home-slideshow'); 
	get_template_part('parts/global/apply-link'); 
	echo '</section>';

	// home about the project
	echo '<section class="hm-section" id="section-1">';
	get_template_part('parts/homepage/home-about-project'); 
	echo '</section>';

	// home financial report
	echo '<section class="hm-section" id="section-2">';
	get_template_part('parts/homepage/home-financial-report'); 
	echo '</section>';

	// project parties
	echo '<section class="hm-section" id="section-3">';
	get_template_part( 'parts/homepage/project-parties' );
	echo '</section>';

	// project activities
	echo '<section class="hm-section" id="section-4">';
	get_template_part( 'parts/homepage/project-activities' );
	echo '</section>';

	// home survey results
	echo '<section class="hm-section" id="section-5">';
	get_template_part('parts/homepage/survey-results'); 
	echo '</section>';

	// home legal framework
	echo '<section class="hm-section" id="section-6">';
	get_template_part('parts/homepage/legal-framework'); 
	echo '</section>';

	// home e-room
	echo '<section class="hm-section" id="section-7">';
	get_template_part('parts/homepage/home-e-room'); 
	echo '</section>';

	// home photo gallery & video
	echo '<section class="hm-section" id="section-8">';
	get_template_part('parts/homepage/home-photo-video'); 
	echo '</section>';

get_footer(); ?>