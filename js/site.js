jQuery(document).ready(function( $ ){


section_dots();
function section_dots(){
	$('.hm-section').each(function( index, value ){
		$(this).attr('id', index);
		$('.dots-container').append('<a href="#'+index+'" class="scroll  section-'+index+'"></a>');
	});
}

section_scroll();
function section_scroll(){
	$('a.scroll').click(function(e){
		e.preventDefault();
		
		$('a.scroll').removeClass('active');
	
		var section = $(this).attr('href');

		
		if(section.length){
			var id = section.replace('#', '');
			console.log(id);
			$('.section-'+id).addClass('active');
			 $('html, body').animate({
		        scrollTop: $(section).offset().top - 0
		    }, 400);
		}
		
	});
}
section_arrows();
function section_arrows(){
	$('.hm-section').each(function( index, value ){
		$(this).attr('id', index);
		$('.arrow-container').append('<i href="#'+index+'" class="scroll  section-'+index+'"></i>');
	});
}


/*
* config
*------------------------------*/
var screen = {
	'window_width'    : $(window).width(),  
	'window_height'   : $(window).height(), 
	'document_width'  : $(document).width(), 
	'document_height' : $(document).height()
};

var device = {
	'tablet_land' : 1024,  // landscape
	'tablet_port' : 768,  // portrait
	'smartphone'  : 767  // smartphone
};

var device = {
	'is_desktop'      : ( screen.window_width > device.tablet_land )  ? true : false,
	'is_tablet_land'  : ( screen.window_width <= device.tablet_land  && screen.window_width >= device.tablet_port ) ? true : false,
	'is_tablet_port'  : ( screen.window_width <= device.tablet_port && screen.window_width >= screen.smartphone ) ? true : false,
	'is_smartphone'   : ( screen.window_width < device.tablet_port) ? true : false
};


/*
* slick sliders
*------------------------------*/

$('.slideshow').slick({
	autoplay       : true,
	autoplaySpeed  : 3000,
	arrows         : false,
	dots           : false,
	infinite       : true,
	speed          : 300,
	slidesToShow   : 1,
	adaptiveHeight : false,
	fade		   : true,
	pauseOnHover   : true,
	pauseOnFocus   : true,
}).find('.item').fadeIn(500);

//home slider
	$('.home-slideshow').slick({
		autoplay: false,
		arrows: false,
	 	dots: true,
	  	infinite: true,
	  	speed: 500,
	  	fade: true,
	 	slidesToShow: 1,
	});	

//initSlideshow
$('.initSlideshow').slick({
	arrows         : true,
	dots           : false,
	infinite       : false,
	speed          : 300,
	slidesToShow   : 3,
	slidesToScroll : 3,
	autoplay       : true,
	pauseOnHover   : false,
	pauseOnHover   : false,
	responsive     : [ 
		{
			breakpoint: 1024 , settings: {slidesToShow : 2, slidesToScroll: 2, infinite: true,	dots: true } },
		{ 	breakpoint: 600  , settings: { slidesToShow: 1, slidesToScroll: 1 } },
		{ 	breakpoint: 480  , settings: { slidesToShow: 1, slidesToScroll: 1 } } 
	]
});


	// blog posts responsive mobile

	if ( device.is_smartphone ) {
		$('.blog-item').each(function(index, value){
		$(this).find('.flip').appendTo($(this).find('.append'));
		});
	 }

	// match height for legal framework content 

	$('.f-content').matchHeight();

	$('.other-frameworks').matchHeight();
	
	$('.assistence-item').matchHeight();
	

	// e-room img match height e-room content
	$('.content').matchHeight({
		target: $('.item')
	});
	// footer match height
	$('.footer-menu, .footer-social').matchHeight({
		target: $('.footer-logo')
	});


	// activity-slider items
	$('.activity-content p').matchHeight();
	// legal framework items
	$('.fr-item:nth-child(3)').matchHeight({
		target: $('.fr-item:nth-child(2)')
	});


	$('.fr-item:nth-child(6)').matchHeight({
		target: $('.fr-item:nth-child(7)')
	});

	//  match height for menu button
	
	$('.menu-btn').matchHeight({
		target: $('.navbar-default')
	});

	// match height for e-room
	
	$('.e-room-entry').matchHeight({
		target: $('.e-room-image')
	});

	// survey results
	
	// $('.u-download-reports').matchHeight({
	// 	target: $('.u-finrep-content')
	// });
	// $('.p-download-reports').matchHeight({
	// 	target: $('.p-finrep-content')
	// });

	// open video
	$('.openVideo').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });


	//display menu overlay  on button click

	// $('.menu-btn').click(function(){
	// 	if ($('#menu-container').hasClass('menu-is-open')){
	// 		$('#menu-container').slideUp("slow");
	// 		$('#menu-container').removeClass('menu-is-open');
	// 		return
	// 	}
	// 	 $('#menu-container').slideDown("slow");
	// 	 $('#menu-container').addClass('menu-is-open');
	// });
	// $('.menu-btn').click(function(){
		 
	// });


	$('.menu-btn').click(function(){
		$('.mobile-menu').toggleClass('hidden');
	})
	$('.close-menu').click(function(){
		$('.mobile-menu').addClass('hidden');
	});


// scroll to top

	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

	/**
	* matchHeight
	*------------------------------*/
	if(device.is_desktop){

	}



	// nicescsroll
	$('.post-cont').niceScroll();
	$('.scroll-content').niceScroll();

	/**
	* nicescroll
	*------------------------------*/
	//nicescroll();
	function nicescroll(){
			$('.nicescroll').niceScroll({
				// cursorcolor: '#ccc',
				cursoropacitymin: 0.5,
				railpadding: { top: 10, right: 3, left: 3, bottom: 10 },
			});
	}

	/**
	* Enllax
	*------------------------------*/
	$(window).enllax();

	/**
	* Media Gallery
	*------------------------------*/
	$('.media-gallery').slick({
		dots: false,
		arrow: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: ( $('.main-content').length ) ? true : false,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					adaptiveHeight: true
				}
			},
		]
	});


	/*
	* scroll_to
	* scroll to target element
	* @param $element (selector)
	*------------------------------*/
	function scroll_to(element){
		var top = (device.is_desktop) ? 54 : 0;
		 $('html, body').animate({
	        scrollTop: $(element).offset().top - top
	    }, 400);
	}


	/*
	* magnific popup
	*------------------------------*/
	$('.popup-gallery').magnificPopup({
	    delegate: 'a',
	    type: 'image',
	    tLoading: 'Loading image #%curr%...',
	    mainClass: 'mfp-img-mobile',
	    gallery: {
	        enabled: true,
	        navigateByImgClick: true,
	        preload: [0,1]
	    },
	    image: {
	    	tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
	        titleSrc: function(item) {
	          return item.el.attr('title') + '<small></small>';
	        }
	    }
	});

	
    // E ROOM GALLERY SLIDER

  $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});

});// document ready