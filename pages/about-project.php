<?php
/**
* Template Name: About the Project
*/
get_header();?>

<?php get_template_part('parts/global/page-title'); ?>

<?php get_template_part('parts/about-project/project-info'); ?>

<?php get_template_part('parts/about-project/first-section'); ?>

<?php get_template_part('parts/about-project/project-data-sheet'); ?>

<?php get_template_part('parts/about-project/development-objectives'); ?> 

<?php get_template_part('parts/about-project/project-components'); ?>

<?php get_footer(); ?>