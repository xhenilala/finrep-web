<?php
/**
* Template Name: Activities
*/
get_header();?>

<?php while( have_posts() ) : the_post() ?>

<?php get_template_part('parts/global/page-title'); 

      get_template_part('parts/project-activities/image-section');
      
      get_template_part('parts/project-activities/technical-assistence');

      get_template_part('parts/project-activities/regulations');

      get_template_part('parts/project-activities/auditing-standarts');

      get_template_part('parts/project-activities/list-activities');

?>

<?php endwhile; ?>


<?php get_footer(); ?>