<?php
/**
* Template Name: E-room
*/
get_header();?>

<?php while (have_posts() ) : the_post() ?>
	 
<?php get_template_part('parts/global/page-title'); 
	  get_template_part( 'parts/e-room/e-room-image' );
	  get_template_part( 'parts/e-room/e-room-content' );
	  // get_template_part('parts/e-room/e-room-event-gallery');
	  get_template_part( 'parts/e-room/e-room-event-video' );
?>

<?php endwhile; ?>
<?php get_footer(); ?>