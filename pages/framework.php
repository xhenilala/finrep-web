<?php
/**
* Template Name: Framework
*/
get_header();?>

<?php get_template_part('parts/global/page-title'); ?>

<?php get_template_part('parts/framework/framework-entry'); ?>

<?php get_template_part('parts/framework/framework-items'); ?>

<?php get_footer(); ?>