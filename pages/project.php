<?php
/**
* Template Name: Project Parties
*/
get_header();?>
    <?php while( have_posts() ) : the_post() ?>
        <?php get_template_part('parts/global/page-title'); ?>
        <?php get_template_part('parts/project-parties/project-parties-content'); ?>
        <?php get_template_part('parts/project-parties/list-parties'); ?>
    <?php endwhile; ?>
<?php get_footer(); ?>