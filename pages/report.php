<?php
/**
* Template Name: Financial Report
*/
get_header();?>

<?php while( have_posts() ) : the_post() ?>

    <?php get_template_part('parts/global/page-title'); 
          
          get_template_part('parts/financial-reporting/financial-report');

          get_template_part('parts/financial-reporting/list-reports');

    ?>

<?php endwhile; ?>

<?php get_footer(); ?>