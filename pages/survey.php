<?php
/**
* Template Name: Survey
*/
get_header();?>

<?php 
    while(have_posts()) : the_post();

        get_template_part('parts/global/page-title');

        get_template_part('parts/survey-results/user-finrep');
        
        get_template_part('parts/survey-results/prepares-finrep'); 

    endwhile;
?>

<?php get_footer(); ?>