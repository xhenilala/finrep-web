<?php	$section_title = get_field('objectives_title'); 
		$objectives_desc = get_field('objectives_section_description');
		$objective_nr = get_field('third_objective_number');
 		$result = get_field('result_title');
 		$img = get_field('pr_objectives_image');
 		$results = get_field('results');?>
<div class="clearfix"></div>
<section class="objective-04">
	<div class="container-fluid">
		<div class="development-objectives col-xs-12">
			<div class="objectives col-sm-6 col-xs-12">
				<h5><?php pll_e('objectives'); ?></h5>
				<p><?php echo apply_filters( 'the_content', $objectives_desc ); ?></p>
				<h6><?php echo $result; ?></h6>
				<?php foreach( $results as $result ) : ?>
				<div class="result-item">
					<?php echo $result['result_nr']; ?>
					<p><?php echo $result['result_description']; ?></p>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="objective-img col-sm-6 col-xs-12">
				<img src="<?php echo get_template_directory_uri() . '/images/small-square.png'; ?>" alt="dots" class="img-responsive small-square">
				<img src="<?php echo get_template_directory_uri() . '/images/about-project-img.png'; ?>" alt="dots" class="img-responsive obj-dots">
				<div class="obj-img has-bg" style="background-image: url(<?php echo $img; ?>);">	
				</div>
				<h2 class="obj-title"><?php echo $section_title; ?></h2>
				<img src="<?php echo get_template_directory_uri() . '/images/obje-lines.png'; ?>" alt="dots" class="img-responsive obj-lines">
				<!-- add objective number -->
				<div class="objective">
					<div class="inner-content">
						<h6><?php echo 'objective'; ?></h6>
						<h1><?php echo $objective_nr; ?></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>