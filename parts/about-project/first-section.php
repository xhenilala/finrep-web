<section class="first-objective tb-padd col-xs-12">
	<div class="container-fluid">
		<?php while( have_posts() ) : the_post(); 
		$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()));?>
		<div class="page-img col-sm-6 col-xs-12">
			<img src="<?php echo get_template_directory_uri() . '/images/about-project-img.png'; ?>" alt="dots" class="img-responsive square-dots">
			<div class="about-pr-img" style="background-image: url(<?php echo $image; ?>);"></div>
			<img src="<?php echo get_template_directory_uri() . '/images/two-squares.png'; ?>" alt="dots" class="img-responsive two-squares">
			<!-- <img src="<?php echo $image; ?>" class="img-responsive"> -->
			<div class="objective">
				<div class="inner-content">
					<h6><?php echo 'objective'; ?></h6>
					<h1><?php echo '01'; ?></h1>
				</div>
			</div>
		</div>
		<div class="page-content col-sm-6 col-xs-12">
			<h4><?php pll_e('repairs'); ?></h4>
			<?php the_content(); ?>
		</div>
		<?php endwhile; ?>
	</div>
</section>