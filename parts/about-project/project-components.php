<?php $section_title = get_field('component_section_title');
	  $objective_nr = get_field('component_objective_number'); ?>
<div class="clearfix"></div>
<section class="objective-5 col-xs-12">
	<div class="container-fluid">
		<div class="objective-4 col-xs-12 ">
			<div class="objective">
				<div class="inner-content">
					<h6><?php pll_e('objective'); ?></h6>
					<h1><?php echo $objective_nr; ?></h1>
				</div>
			</div>
			<span><?php echo $section_title; ?></span>
		</div>
	</div>
	<div class="container-fluid">
		<?php $i = 1;  ?>
		<?php while( have_rows('components') ): the_row(); ?>
			<div class="pr-component col-xs-12">
				<div class="count-component col-xs-12">
					<div class="count-components"><p>
						<?php $i< 10 ? print 0 : false; ?><?php echo $i; ?></p></div>
					<?php while( have_rows('single_component') ): the_row(); ?>
						<div class="inner-pr-component  col-xs-12">
							<h2><?php echo the_sub_field('component_title'); ?></h2>
							<p><?php echo the_sub_field('component_description'); ?></p>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		<?php $i++; ?>
		<?php endwhile; ?>
	</div>
</section>