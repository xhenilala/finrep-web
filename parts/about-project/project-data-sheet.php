<?php $section_title = get_field('second_section_title');
	  $objective_nr = get_field('second_objective_number');
	  $data = get_field('data_sheets');
	  $nr = 1; ?>
<section class="second-objective col-xs-12">
	<div class="container-fluid">
		<div class="objective-2 col-xs-12">
			<span><?php echo $section_title; ?></span>
			<div class="objective">
				<div class="inner-content">
					<h6><?php echo 'objective'; ?></h6>
					<h1><?php echo $objective_nr; ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="data-table col-xs-12">
			<div class="data-item-title col-xs-12 rmp table-responsive">
				<table class="table">
					<tr>
						<td style="width: 140px;"><p><?php pll_e('No.'); ?></p></td>
						<td style="width: 320px;"><p><?php pll_e('Category'); ?></p></td>
						<td><p><?php pll_e('Description'); ?></p></td>
					</tr>
				</table>
				<table class="table">
					<?php foreach( array_chunk($data,1) as $data_item): ?>
						<tr class="data-item">
							<td style="width: 140px;"><div class="nr  "><?php echo $nr.'.'; ?></div></td>
							<td style="width: 320px;"><div class="cat  "><?php echo $data_item[0]['category_title']; ?></div></td>
							<td><div class="desc "><?php echo $data_item[0]['description']; ?></div></td>
						</tr>
						<?php $nr++; ?>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>
</section>