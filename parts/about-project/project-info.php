<?php $title = get_field('info_title');
	  $add_info = get_field('add_info'); ?>
<section class="pr-info">
	<div class="container-fluid">
		<div class="row">
			<div class="info-content">
				<h1 class="title"><?php echo $title;?></h1>
				<div class="info-description">
					<?php if($add_info) :?>
						<?php foreach ($add_info as $item): ?>
							<div class="col-sm-6 col-xs-12">
								<div class="info-item">
									<?php echo $item['info']; ?>	
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
