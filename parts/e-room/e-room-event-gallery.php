<?php 
	$events = \App\Query::get_events_gallery(10);
if ($events): ?>	
<section class="event-gallery">
	<div class="container-fluid">
		<div class="row event-gallery-items">
			<div class="slideshow-gallery col-sm-6">
				<!-- <img src="<?php echo get_template_directory_uri() . '/images/event-gallery-shape.png'; ?>" alt="dots" class="e-room-shape"> -->
				<?php foreach ($events as $event): 
					$gallery = $event['gallery'];
					foreach( $gallery as $item ):
						$imgs[]['src'] = $item['url'];
					endforeach;
					$thumbnail = $event['gallery'][0]['url']; ?>
					<div class="item-gallery open-gallery" data-gallery = '<?php print json_encode( $imgs ); ?>'>
						<h4 class="item-title"><?php echo $event['post_title']; ?></h4>
						<img src="<?php echo $thumbnail; ?>" class = "img-responsive item-photo">	
					</div>	
				<?php unset($imgs);
				endforeach; ?>
			</div>
			<div class="col-sm-6">
				<div class="slideshow-gallery-nav ">
				<?php foreach ($events as $event):
				$thumbnail = $event['gallery'][0]['sizes']['thumbnail']; ?>
					<div class="item-gallery">
						<h4 class="item-title"><?php echo $event['post_title']; ?></h4>
						<h6 class="item-date"><?php echo $event['event_date']; ?></h6>
						<img src="<?php echo $thumbnail; ?>" class = "img-responsive">		
					</div> 
				<?php endforeach; ?>
				</div>	
			</div>
					
		</div>
	</div>
</section>
<?php endif; ?>
