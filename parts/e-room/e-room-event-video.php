<?php 
$videos = \App\Query::get_events_videos(10);
if( $videos ) : ?>
    <section class="e-event-videos">
        <div class="container">
            <div class="e-video-container video-player">
                <div class="video-container">
                    <div class="video">
                        <iframe
                            id="ytplayer"
                            class="video-iframe"
                            src=""
                            frameborder="0">
                        </iframe>
                    </div>
                    <div class="img-video-shape poz-b-l">
                      <img src="<?php echo get_template_directory_uri() . '/images/red-event-shape.png'; ?>" alt="dots" class="img-responsive e-room-shape">  
                    </div>
                </div>
                <h2 class="event-title"></h2>
                <h5 class="event-content"></h5>
                <div class="event-date text-center"></div>
                <div class="hr text-center"></div>
               
            </div>
        </div>
        <div class="container">
            <div class="initSlideshow">
                <?php 
                foreach( $videos as $video ) :  ?>
                    <div class="video-item col-sm-4" 
                        data-video-id="<?php echo $video['video_id']; ?>"
                        data-video-title="<?php echo $video['post_title']; ?>" 
                        data-video-content="<?php echo $video['post_content']; ?>" 
                        data-video-date="<?php echo $video['event_date']; ?>">
                        <div class="event-description col-xs-12">
                            <img src="<?php echo $video['thumbnail']; ?>" class="img-responsive">
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section><?php 
endif; ?>
<script>
    jQuery(document).ready(function($){

        var player = $('.video-player iframe');

        $(window).load(function(){
            $('.video-item:nth-child(1)').trigger('click');
        });

        $('.video-item').click(function(){
            var _this = $(this);
            var event = {
                title    : _this.data('video-title'),
                content  : _this.data('video-content'),
                date     : _this.data('video-date'),
                vid      : _this.data('video-id')
            };
            console.log(event);

            load_video( event.vid );
            set_player_title( event.title );
            set_player_date( event.content );
            set_player_content( event.date );
        });
        function load_video( vid ){  $(player).attr('src', 'https://www.youtube.com/embed/'+vid+'?autoplay=1'); }
        function set_player_title( string ){  $('.event-title').text(string); }
        function set_player_date( string ){ $('.event-date').text(string); }
        function set_player_content( string ){ $('.event-content').text(string); }  
    });
</script>