<?php $img = \App\Utils::get_thumbnail_url();  ?>
<section class="e-section">
    <div class="container-fluid">
        <div class="e-outer-container col-xs-12">
            <div class="contains-img e-room-container">
                <img src="<?php echo get_template_directory_uri() . '/images/grey-dots.png'; ?>" alt="dots" class="img-responsive poz-t-r  e-room-shape">
                <div class="img-container has-bg" style="background-image: url(<?php echo $img; ?>)">
                </div>
                <div class="square square-red poz-b-l"></div>
            </div>
        </div>
    </div>
</section>