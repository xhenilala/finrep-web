<section class="e-room-socials">
	<div class="container">
		<?php $socials = get_field( 'socials','options' ); 
		if( $socials ):
			foreach( $socials as $social ):?>
				<div class="col-sm-6">
					<div class="row">
						<a href="<?php echo $social['social_url']; ?>" class="social-item">
							<i class="<?php echo $social['social_icon']; ?> social-icons" aria-hidden="true"><?php echo $social['social_name'] ?></i>
						</a>
						<div class="cirlce circle-socials"></div>
					</div>
				</div>		
			<?php endforeach;?>
		<?php endif; ?>
	</div>
</section>