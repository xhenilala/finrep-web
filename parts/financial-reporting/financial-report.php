<?php $img = \App\Utils::get_thumbnail_url(); 
      $reporting = get_field('reporting'); ?>
<div class="clearfix"></div>
<section class="finrep">
    <div class="container-fluid">
        <div class="finrep-page-entry col-xs-12">
            <?php the_content(); ?>
        </div>
        <div class="finrep-page-content col-xs-12 rmp">
            <div class="financial-img col-sm-6 col-xs-12">
                <img src="<?php echo $img ; ?>" alt="<?php the_title(); ?>" class="img-responsive fin-img">
                <img src="<?php echo get_template_directory_uri() . '/images/grey-dots.png'; ?>" alt="dots" class="img-responsive grey-dots">
            </div>
            <div class="financial-importance finance-content col-sm-6 col-xs-12">
                <?php foreach ( $reporting as $report ): ?>
                    <ul>
                        <li><?php echo $report['fin_importance']; ?></li>
                    </ul>
                <?php endforeach; ?>
                <img src="<?php echo get_template_directory_uri() . '/images/two-red-squares.png'; ?>" alt="dots" class="img-responsive red-sq">
            </div>
        </div>
    </div>
</section>