<?php $fin_reporting = get_field('fin_reporting'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="finrep-list-technical-assistence col-xs-12">
            <?php foreach( $fin_reporting as $report ) : ?>
                <div class="assistence-item-report col-xs-12">
                    <ul>
                        <li><?php echo $report['list']; ?></li>
                    </ul>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>