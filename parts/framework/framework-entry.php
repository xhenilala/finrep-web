<?php $page_entry = get_field('page_entry');
	  $short_desc = get_field('short_description'); ?>
<section class="framework-page col-xs-12">
	<div class="container-fluid">
		<?php while(have_posts()) : the_post(); 
		$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
		<div class="framework-entry col-xs-12">
			<?php if($page_entry) : ?>
				<?php echo apply_filters( 'the_content', $page_entry );?>
			<?php endif; ?>
			<div class="f-img col-xs-12">
				<img src="<?php echo get_template_directory_uri() . '/images/fr-squares.png'; ?>" alt="dots" class="img-responsive fr-squares">
				<div class="frame-img has-bg" style="background-image: url(<?php echo $img_src; ?>);"></div>
				<img src="<?php echo get_template_directory_uri() . '/images/about-project-img.png'; ?>" alt="dots" class="img-responsive fr-dots">
			</div>
			<div class="clearfix"></div>
			<?php if( $short_desc ) : ?>
				<div class="fr-short-description col-xs-12">
					<img src="<?php echo get_template_directory_uri() . '/images/about-project-img.png'; ?>" alt="dots" class="img-responsive">
					<?php echo apply_filters( 'the_content', $short_desc );?>
				</div>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
	</div>
</section>
<div class="clearfix"></div>