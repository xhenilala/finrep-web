<?php $query= \App\Query::legal(); 
	if( $query->have_posts() ) : $i = 0; ?>
	<section class="framework-items col-xs-12">
		<?php while( $query->have_posts() ) : $query->the_post(); ?>
			<div class="fr-item col-sm-6 col-xs-12">	
				<div class="container-fluid">
					<div class="fr-item-content">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php   endwhile; ?>
	</section><?php wp_reset_query();
endif; ?> 