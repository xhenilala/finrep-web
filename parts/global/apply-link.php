<?php $link = get_field('apply_link_'.pll_current_language(), 'options');  ?>
<div class="clearfix"></div>
<div class="link-section col-xs-12">
    <div class="apply">
        <a href="<?php echo $link; ?>"><?php pll_e('apply now'); ?></a>
        <div class="apply-link-dots">
            <img src="<?php echo get_template_directory_uri() . '/images/apply-dots.png'; ?>" alt="dots" class="img-responsive apply-dots">
        </div>
    </div>
</div>
