<?php $subtitle = get_field('subtitle'); ?>
<div class="section-title col-xs-12">
	<p><?php echo the_title(); ?></p>
	<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
	<h3><?php _e('|'); ?></h3>
	<?php if($subtitle): ?>
		<h4><?php echo $subtitle; ?></h4>
	<?php  endif; ?>
</div>