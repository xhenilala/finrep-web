<?php $socials = get_field( 'socials','options' ); 
if( $socials ):
	foreach( $socials as $social ):?>
		<a href="<?php echo $social['social_url']; ?>" class="social-item">
			<i class="<?php echo $social['social_icon']; ?>" aria-hidden="true"></i>
		</a>
	<?php endforeach;?>
<?php endif; ?>