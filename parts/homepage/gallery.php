<?php $h_gallery = get_field('home_gallery','options'); ?>

<div class="gallery col-xs-12">
	<div class="fotorama" 
		 data-loop="true"
		 data-width="80%"
     	 data-ratio="600/400" >
		<?php foreach( $h_gallery as $gallery): ?>
			<img src="<?php echo $gallery['sizes']['large']; ?>">
		<?php endforeach; ?>
	</div>
</div>