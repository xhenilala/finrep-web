<?php $project_link = get_field('about_project_link_'.pll_current_language(),'options');
	  $image =get_field('pr_image' , 'options'); 
	  $project_desc = get_field('project_description_'.pll_current_language(),'options');
	  $project_image = get_field('project_image','options');
	  $short_pr_desc = get_field('short_pr_description_'.pll_current_language(),'options');?>
<div class="section-padding">
<section class="home-about-project has-bg">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<p><?php echo $project_link->post_title; ?></p>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
		<div class="about-project-page col-sm-6 col-xs-12">
		<div class="about-img-container">
			<a href="<?php echo the_permalink($project_link->ID); ?>">
				<div class="inner-img-container has-bg" style="background-image: url(<?php echo $image; ?>);"></div>
			</a>
			<img src="<?php echo get_template_directory_uri() . '/images/test-sq.png'; ?>" alt="dots" class="img-responsive home-about-squares">
		</div>
			<div class="objective">
				<div class="inner-content">
					<h6><?php pll_e('objective'); ?></h6>
					<h1><?php printf('01'); ?></h1>
				</div>
			</div>
			<p><?php echo $short_pr_desc; ?></p>
			<div class="single-link">
				<a href="<?php echo the_permalink($project_link->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="about-project-description col-sm-6 col-xs-12">
			<p><?php echo apply_filters( 'the_content', $project_desc ); ?></p>
			<a href="<?php echo the_permalink($project_link->ID); ?>">
				<img src="<?php echo $project_image; ?>" alt="<?php echo $project_link->post_title; ?>" class="img-responsive">
			</a>
		</div>
	</div>
</section>
</div>

