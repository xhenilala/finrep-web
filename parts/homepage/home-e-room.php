<?php $room_link = get_field('e-room_link_'.pll_current_language(),'options');
	   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $room_link->ID ),'full', false ); 
	   $room_title = get_field('e_room_title_'.pll_current_language(),'options'); 
	   $room_description = get_field('e_room_desc_'.pll_current_language(),'options'); ?>
<section class="home-room col-xs-12">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<p><?php echo $room_link->post_title; ?></p>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="'dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
		<div class="e-room-entry col-sm-4 col-xs-12">
			<div class="center-content">
				<h4><?php echo $room_title; ?></h4>
				<?php echo apply_filters('the_content' , $room_description ); ?>
				<div class="single-link">
					<a href="<?php echo the_permalink($room_link->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		<div class="e-room-image col-sm-8 col-xs-12">
			<img src="<?php echo get_template_directory_uri() . '/images/top-right.png'; ?>" alt="dots" class="img-responsive e-top-shape">
			<div class="e-image has-bg" style="background-image: url('<?php echo $image[0]; ?>');"></div>
			<!-- <img src="<?php echo $image[0]; ?>" class="img-responsive" alt="<?php $room_link->post_title; ?>"> -->
			<img src="<?php echo get_template_directory_uri() . '/images/bottom-left.png'; ?>" alt="dots" class="img-responsive e-shape">
		</div>
	</div>
</section>