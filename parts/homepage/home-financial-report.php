<?php $financial_report = get_field('financial_report_'.pll_current_language(),'options');
	  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $financial_report->ID ),'full', false );
	  $finance_title = get_field('finance_title_'.pll_current_language(),'options'); 
?>
<section class="home-financial-report">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo apply_filters( 'the_content', $finance_title ); ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
		<div class="finance-image col-sm-6 col-xs-12">
			<img src="<?php echo get_template_directory_uri() . '/images/grey-lines.png'; ?>" alt="dots" class="img-responsive grey-lines">
			<a href="<?php echo the_permalink($financial_report->ID); ?>">
				<img src="<?php echo $image[0]; ?>" class="img-responsive statistic-img" alt="finrep">
			</a>
		</div>
		<div class="finance-content col-sm-6 col-xs-12">
			<!-- <img src="<?php echo get_template_directory_uri() . '/images/squares.png'; ?>" alt="'dots" class="img-responsive grey-squares"> -->
			<?php echo apply_filters('the_content' , $financial_report->post_content ); ?>
			<div class="single-link">
				<a href="<?php echo the_permalink($financial_report->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>	
</section>