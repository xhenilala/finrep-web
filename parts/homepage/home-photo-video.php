<?php $gallery = get_field('home_gallery','options'); 
	  $video = get_field('video','options', false, false); 
	  $title = get_field('photo_video_title_'.pll_current_language(),'options'); ?>
<div class="clearfix"></div>
<section class="home-photos-videos contain-bg col-xs-12">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo apply_filters( 'the_content', $title ); ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="'dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
		<div class="photo-gallery col-sm-6 col-xs-12">
			<?php get_template_part('parts/homepage/gallery'); ?>
		</div>
		<div class="home-video col-sm-6 col-xs-12">
			<a href="<?php echo $video; ?>" class="openVideo" >
			<div class="home-about-video has-bg col-xs-12 rmp" style="background-image: url(<?php echo \App\Utils::getVideoThumbnail($video);?>);">
				<!-- <i class="fa fa-play" aria-hidden="true"></i> -->
				<img src="<?php echo get_template_directory_uri() . '/images/play-icon.png'; ?>" alt="play-video" class="img-responsive">
				<div class="video-overlay"></div>
			</div>
			</a>
		</div>
	</div>
</section>
