<?php 
$query = \App\Query::slideshow();
if( $query->have_posts() ) : ?>
	<section class="home-slideshow ">
		<?php while( $query->have_posts() ) : $query->the_post(); 
			$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
			<div class="item has-bg-slide col-xs-12 rmp" style="background-image: url(<?php echo $img_src;?>)">
				<div class="content col-xs-12 col-sm-5 rmp">
					<div class="slide-dots">
						<img src="<?php echo get_template_directory_uri() . '/images/red-dots.svg'; ?>" alt="dots" class="img-responsive red-dots">
					</div>
					<div class="slide-content">
						<?php apply_filters('the_content',the_content()); ?>
						<img src="<?php echo get_template_directory_uri() . '/images/slide-cont-lines.png'; ?>" alt="dots" class="img-responsive slide-lines">
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</section><?php 
	wp_reset_query();
endif; ?>
<div class="clearfix"></div>

