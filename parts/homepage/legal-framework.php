<?php 
$query = \App\Query::legal();

$legal_title = get_field('legal_framework_'.pll_current_language(),'options');

$legal_link = get_field('legal_framework_link_'.pll_current_language(),'options');

if( $query->have_posts() ) : 
$i = 0; ?>
<section class="home-legal-framework col-xs-12 rmp">
	<!-- <img src="<?php echo get_template_directory_uri() . '/images/side-lines.png'; ?>" alt="'dots" class="img-responsive side-lines"> -->
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo apply_filters( 'the_content', $legal_link->post_title ); ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
	<div class="legal-framework col-sm-3 col-xs-12">
		<div class="centered-title">
			<div class="title">
				<?php echo apply_filters( 'the_content', $legal_title ); ?>
			</div>
		</div>
	</div>
	<?php while ( $query->have_posts() ) : $query->the_post() ;
	$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	if ($i <= 2) : ?>
	<div class="framework col-md-3 col-sm-6 col-xs-12">
		<div class="framework-img-title">
			<img src="<?php echo $img_src; ?>" alt="<?php the_title(); ?>" class="img-responsive parties-img">
			<h4><?php the_title(); ?></h4>
		</div>
		<div class="f-content">
			<p><?php echo wp_trim_words(get_the_content(), '20', '...'); ?></p>
		</div>
		<div class="single-link">
			<a href="<?php echo get_the_permalink( $legal_link->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		</div>
	</div>
	<?php else : ?>
		<div class="other-frameworks col-md-3 col-sm-6 col-xs-12">
			<div class="framework-img-title">
				<img src="<?php echo $img_src; ?>" alt="<?php the_title(); ?>" class="img-responsive parties-img">
				<h4><?php the_title(); ?></h4>
			</div>
			<div class="f-content">
				<p><?php echo wp_trim_words(get_the_content(), '20', '...'); ?></p>
			</div>
			<div class="single-link">
				<a href="<?php echo get_the_permalink( $legal_link->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	<?php endif; ?>
	<?php $i++; ?>
	<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>