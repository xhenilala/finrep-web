
<?php $title = get_field('project_activities_title_'.pll_current_language(),'options'); 
	  $section_link = get_field('pr_activities_link_'.pll_current_language() , 'options');
	  $image   = wp_get_attachment_image_src( get_post_thumbnail_id( $section_link->ID ),'full', false );
	  $section_description = get_field('project_activities_desctiption_'.pll_current_language() , 'options');
?>
<div class="clearfix"></div>
<section class="home-project-activities col-xs-12">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo $title; ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
			<h4><?php pll_e('components'); ?></h4>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="container-fluid">
		<div class="pr-activities col-xs-12">
			<div class="pr-activities-img col-sm-6 col-xs-12 rmp">
				<a href="<?php echo the_permalink($section_link->ID); ?>">
					<img src="<?php echo $image[0]; ?>" alt="<?php $section_link->post_title; ?>" class="img-responsive activity-image">
				</a>
			</div>
			<div class="pr-activities-desc col-sm-6 col-xs-12">
				<div class="pr-desc">
					<?php echo apply_filters('the_content' , $section_description ); ?>
					<div class="single-link">
						<a href="<?php echo the_permalink($section_link->ID); ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>