<?php 
$query = \App\Query::parties();

$section_title = get_field('project_parties_'.pll_current_language(),'options');

$section_link = get_field('section_link_'.pll_current_language(),'options');

if( $query->have_posts() ) : 
$i = 0; ?>

<section class="home-project-parties has-bg col-xs-12 rmp">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo apply_filters( 'the_content', $section_link->post_title ); ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
	<div class="projects col-sm-3 col-xs-12">
		<div class="centered-title">
			<div class="title">
				<?php echo apply_filters( 'the_content', $section_title ); ?>
			</div>
			<div class="s-link">
				<a href="<?php echo the_permalink( $section_link ); ?>"><?php pll_e('see more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<?php while ( $query->have_posts() ) : $query->the_post() ;
	$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	if ($i <= 2) : ?>
	<div class="members col-md-3 col-sm-3 col-xs-12">
	<a href="<?php echo the_permalink( $section_link ); ?>">	
		<div class="white-bg">
			<img src="<?php echo $img_src; ?>" alt="<?php the_title(); ?>" class="img-responsive parties-img">
		</div>
	</a>
	</div>
	<?php else : ?>
		<div class="other-members col-md-3 col-sm-3 col-xs-12">
		<a href="<?php echo the_permalink( $section_link ); ?>">	
			<div class="white-bg">
				<img src="<?php echo $img_src; ?>" alt="<?php the_title(); ?>" class="img-responsive parties-img">
			</div>
		</a>
		</div>
	<?php endif; ?>
	<?php $i++; ?>
	
	<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>