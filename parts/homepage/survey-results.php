<?php $survey_link = get_field('survey_link_'.pll_current_language(),'options');
      $image =  wp_get_attachment_image_src( get_post_thumbnail_id( $survey_link->ID ),'full', false );
      $financial_title = get_field('financial_title_'.pll_current_language(),'options');
      $finance_link = get_field('finance_link_'.pll_current_language(),'options');
	  $finance_image = get_field('finance_image','options');
	  $user_survey_desc = get_field('hp_user_finrep_desc_'.pll_current_language(),'options');
	  $user_survey_title = get_field('hp_user_finrep_'.pll_current_language() , 'options'); ?>
<section class="home-survey-result section-padding">
	<div class="container-fluid">
		<div class="section-title col-xs-12">
			<?php echo apply_filters( 'the_content', $survey_link->post_title ); ?>
			<img src="<?php echo get_template_directory_uri() . '/images/red-lines.png'; ?>" alt="'dots" class="img-responsive red-lines">
			<h3><?php _e('|'); ?></h3>
		</div>
	</div>
	<div class="container-fluid">
		<div class="survey-container col-sm-6 col-xs-12">
			<div class="survey-page-title">
			<h4><?php echo apply_filters('the_content', $user_survey_title); ?></h4>
			<p><?php echo $user_survey_desc; ?></p>
			<div class="single-link">
				<a href="<?php the_permalink($survey_link->ID); ?>"><?php _e('See more &nbsp;&nbsp;','finance'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="survey-page-img col-sm-6 col-xs-12">
			<img src="<?php echo get_template_directory_uri() . '/images/big-square.png'; ?>" alt="'dots" class="img-responsive big-square">
			<div class="survey-image has-bg" style="background-image: url('<?php echo $image[0]; ?>');"></div>
			<img src="<?php echo get_template_directory_uri() . '/images/slide-cont-lines.png'; ?>" alt="dots" class="img-responsive bottom-lines">
		</div>
		</div>
		<div class="survey-description col-sm-6 col-xs-12">
			<div class="image-cover">
				<!-- <div class="fin-image has-bg" style="background-image: url(<?php echo $finance_image; ?>);"> -->
					<img src="<?php echo $finance_image; ?>" class="img-responsive">
				<!-- </div> -->
			</div>
			<div class="survey-title">
				<?php echo $financial_title; ?>
				<img src="<?php echo get_template_directory_uri() . '/images/short-lines.png'; ?>" alt="short lines" class="img-responsive">
			</div>
			<div class="single-link">
				<a href="<?php echo $finance_link; ?>"><?php pll_e('See more &nbsp;&nbsp;'); ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
</section>
<img src="<?php echo get_template_directory_uri() . '/images/hp-survey-shape.png'; ?>" alt="shape" class="img-responsive survey-shape">