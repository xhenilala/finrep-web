<?php  $title = get_field('improve_auditing_title'); 
       $description =get_field('improve_auditing_content');  
       $img = get_field('auditing_image'); ?>
<section class="forth-section">
    <div class="container-fluid">
        <div class="auditing-section-content col-xs-12">
            <div class="auditing-content col-sm-6 col-xs-12 rmp">
                <h1><?php echo $title; ?></h1>
                <?php echo apply_filters('the_content', $description); ?>
            </div>
            <div class="auditing-img-container col-sm-6 col-xs-12">
                <img src="<?php echo get_template_directory_uri() . '/images/red-dots.svg'; ?>" alt="dots" class="img-responsive auditing-dots">
                <div class="auditing-img has-bg" style="background-image: url(<?php echo $img; ?>)"></div>
            </div>
            <img src="<?php echo get_template_directory_uri() . '/images/grey-lines.png'; ?>" alt="lines" class="img-responsive auditing-lines">
        </div>
    </div>
</section>