<?php $img = \App\Utils::get_thumbnail_url();  ?>
<section class="first-section">
    <div class="container-fluid">
        <div class="outer-container col-xs-12">
            <div class="contains-img">
                <img src="<?php echo get_template_directory_uri() . '/images/red-shape.png'; ?>" alt="dots" class="img-responsive right-top-corner">
                <div class="img-container has-bg" style="background-image: url(<?php echo $img; ?>)">
                </div>
                <img src="<?php echo get_template_directory_uri() . '/images/obje-lines.png'; ?>" alt="dots" class="img-responsive left-bottom-corner">
            </div>
        </div>
    </div>
</section>