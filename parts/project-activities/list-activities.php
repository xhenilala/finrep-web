<section class="list-components">
    <?php $i = 1;  ?>
    <?php while( have_rows('list_components') ): the_row(); ?>
    <div class="list-item"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="activity-entry">
                        <div class="container-fluid">
                            <div class="row activity-content">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="a-img">
                                        <div class="objective">
                                            <div class="inner-content">
                                                <h6 class="components"><?php pll_e('component'); ?></h6>
                                                <h1 class="components-counter"><?php echo '0'.$i; ?></h1>
                                            </div>
                                        </div>
                                        <img src="<?php echo the_sub_field('image');?>" alt="<?php echo the_sub_field('title');?>" class="img-responsive">
                                        <img src="<?php echo get_template_directory_uri() . '/images/red-event-shape.png'; ?>" alt="dots" class="img-responsive red-event-shape">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="a-entry ">
                                        <h3><?php echo apply_filters('the_content',the_sub_field('title'));?></h3>
                                        <?php echo apply_filters('the_content',the_sub_field('entry_text'));?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>        
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="activity-desc">
                                        <?php echo apply_filters('the_content',the_sub_field('description'));?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <?php $i++; ?>
    <?php endwhile; ?>
</section>