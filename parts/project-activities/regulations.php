<?php  $title = get_field('drafting_title'); 
       $description =get_field('drafting_bylaws_regulations');  ?>
<section class="third-section tb-padd">
    <div class="container-fluid">
        <div class="drafting-regulations-content col-xs-12">
            <h1><?php echo $title; ?></h1>
            <?php echo apply_filters('the_content', $description); ?>
        </div>
    </div>
    <img src="<?php echo get_template_directory_uri() . '/images/regulations-lines.png'; ?>" alt="lines" class="img-responsive _lines">
</section>