<?php  $title = get_field('technical_assistence_title'); 
       $description =get_field('technical_assistence_desc'); 
       $assistences = get_field('assistences'); ?>
<section class="second-section">
    <div class="container-fluid">
        <div class="assistence-content col-xs-12">
            <h1><?php echo $title; ?></h1>
            <?php echo apply_filters('the_content', $description); ?>
        </div>
        <div class="list-technical-assistence col-xs-12">
            <?php foreach( $assistences as $assistence ) : ?>
                <div class="assistence-item col-sm-6 col-xs-12">
                    <ul>
                        <li><?php echo $assistence['technical_assistence']; ?></li>
                    </ul>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>