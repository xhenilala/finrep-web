<?php 
$banner = get_field('banner');
$args = [
	'post_type' => 'parties',
	'posts_per_page' => -1,
	];
$posts = get_posts( $args );
$posts = array_chunk($posts, 1); $i = 0; ?>
<section class="list-project-parties col-xs-12">
	<div class="container-fluid">
		<?php foreach( $posts as $post ): 
		if($i == 3 ) :?>
		<div class="parties-banner has-bg col-xs-12 rmp" style="background-image:url(<?php echo $banner; ?>);">
		</div>
		<div class="clearfix"></div>
	<?php endif; ?>
		<?php $img_src = wp_get_attachment_url(get_post_thumbnail_id( $post[0]->ID)); ?>
		<div class="item-parties col-xs-12">
			<?php foreach( $post as $chunk ): ?>
			<div class="partie-image col-sm-3 col-xs-12">
				<div class="_img"><img src="<?php echo $img_src; ?>" class="img-responsive" alt="<?php echo $chunk->post_title; ?>"></div>
			</div>
			<div class="partie-description col-sm-9 col-xs-12">
				<h2><?php echo $chunk->post_title; ?></h2>
				<?php echo apply_filters('the_content',$chunk->post_content); ?>
			</div>
			<?php endforeach; ?>
		</div>
		<?php $i++; endforeach; ?>
	</div>
</section>