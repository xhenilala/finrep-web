<?php $title = get_field('prepare_finrep_title');
      $desc = get_field('prepare_finrep_desc');
      $usr_reports = get_field('prepare_financial_reports'); 
?>
<section class="prepares-finrep" id="prepares-reports">
    <div class="container-fluid">
        <div class="p-reports col-xs-12 tb-padd rmp">
            <div class="p-finrep-content col-sm-6 col-xs-12">
                <h1><?php echo $title; ?></h1>
                <?php echo apply_filters('the_content' , $desc ); ?>
            </div>
            <div class="p-download-reports col-sm-6 col-xs-12">
                <h2><?php echo pll_e('Downloads'); ?></h2>
                <?php foreach( $usr_reports as $report ):  ?>
                    <div class="colored-report-bg">
                        <h3><?php echo pll_e('prepares financial report'); ?></h3>
                        <p><?php echo $report['upload_finrep']['filename']; ?></p>
                        <a href="<?php echo $report['upload_finrep']['url']; ?>" download="<?php echo $report['upload_finrep']['filename']; ?>">
                            <?php pll_e('download'); ?> <i class="fa fa-download"></i>
                        </a>
                    </div>
    	        <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
