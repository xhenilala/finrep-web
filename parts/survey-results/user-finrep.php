<?php $title = get_field('user_finrep_title');
      $desc = get_field('user_finrep_desc');
      $usr_reports = get_field('user_financial_reports'); 
?>

<section class="user-finrep" id="user-reports">
    <div class="container-fluid">
        <div class="u-reports col-xs-12 tb-padd rmp">
            <div class="u-finrep-content col-sm-6 col-xs-12">
                <h1><?php echo $title; ?></h1>
                    <?php echo apply_filters('the_content' , $desc ); ?>
            </div>
            <div class="u-download-reports col-sm-6 col-xs-12">
                <h2><?php echo pll_e('Downloads'); ?></h2>
                <?php foreach( $usr_reports as $report ):  ?>
                    <div class="colored-bg">
                        <h3><?php echo pll_e('user financial report'); ?></h3>
                        <p><?php echo $report['upload_report_user']['filename']; ?></p>
                        <a href="<?php echo $report['upload_report_user']['url']; ?>" download="<?php echo $report['upload_report_user']['filename']; ?>">
                           <?php pll_e('user financial report'); ?><i class="fa fa-download"></i>
                        </a>
                    </div>
    	        <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>