<?php

/**
*  event_init
*/
function event_init() {
	register_post_type( 'Event', array(
		'labels'                  => array(
		'name'                    => __( 'Event'		  	  	  , 'finrep' ),
			'singular_name'       => __( 'Event'		  	  	  , 'finrep' ),
			'all_items'           => __( 'Event'  	 		 	  , 'finrep' ),
			'new_item'            => __( 'New Event'			  , 'finrep' ),
			'add_new'             => __( 'Add Event'		 	  , 'finrep' ),
			'add_new_item'        => __( 'Add New event'	      , 'finrep' ),
			'edit_item'           => __( 'Edit item'			  , 'finrep' ),
			'view_item'           => __( 'View item'			  , 'finrep' ),
			'search_items'        => __( 'Search items'			  , 'finrep' ),
			'not_found'           => __( 'No item found'		  , 'finrep' ),
			'not_found_in_trash'  => __( 'No item found in trash' , 'finrep' ),
			'parent_item_colon'   => __( 'Parent item'			  , 'finrep' ),
			'menu_name'           => __( 'Event'	  	  	      , 'finrep' ),
		),
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail'),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
	) );

}
// init
add_action( 'init', 'event_init', 20 );