<?php

/**
*  legal_init
*/
function legal_init() {
	register_post_type( 'legal', array(
		'labels'            => array(
			'name'                => __( 'Legal Acts'		      , 'finance' ),
			'singular_name'       => __( 'Legal Acts'		      , 'finance' ),
			'all_items'           => __( 'Legal Acts'  	          , 'finance' ),
			'new_item'            => __( 'New item'		  		  , 'finance' ),
			'add_new'             => __( 'Add New'				  , 'finance' ),
			'add_new_item'        => __( 'Add New item'			  , 'finance' ),
			'edit_item'           => __( 'Edit item'			  , 'finance' ),
			'view_item'           => __( 'View item'			  , 'finance' ),
			'search_items'        => __( 'Search items'			  , 'finance' ),
			'not_found'           => __( 'No item found'		  , 'finance' ),
			'not_found_in_trash'  => __( 'No item found in trash' , 'finance' ),
			'parent_item_colon'   => __( 'Parent item'			  , 'finance' ),
			'menu_name'           => __( 'Legal Acts'	  	      , 'finance' ),
		),
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail'),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'			=> 'dashicons-format-aside'
	) );

}
// init
add_action( 'init', 'legal_init', 20 );