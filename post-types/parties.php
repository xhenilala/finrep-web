<?php

/**
*  parties_init
*/
function parties_init() {
	register_post_type( 'parties', array(
		'labels'            => array(
			'name'                => __( 'Project parties'		  , 'finance' ),
			'singular_name'       => __( 'Project parties'		  , 'finance' ),
			'all_items'           => __( 'Project parties'  	  , 'finance' ),
			'new_item'            => __( 'New item'				  , 'finance' ),
			'add_new'             => __( 'Add New'				  , 'finance' ),
			'add_new_item'        => __( 'Add New item'			  , 'finance' ),
			'edit_item'           => __( 'Edit item'			  , 'finance' ),
			'view_item'           => __( 'View item'			  , 'finance' ),
			'search_items'        => __( 'Search items'			  , 'finance' ),
			'not_found'           => __( 'No item found'		  , 'finance' ),
			'not_found_in_trash'  => __( 'No item found in trash' , 'finance' ),
			'parent_item_colon'   => __( 'Parent item'			  , 'finance' ),
			'menu_name'           => __( 'Project stakeholders'	  , 'finance' ),
		),
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor','thumbnail'),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'			=> 'dashicons-format-aside'
	) );

}
// init
add_action( 'init', 'parties_init', 20 );