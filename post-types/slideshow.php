<?php

/**
*  slideshow_init
*/
function slideshow_init() {
	register_post_type( 'slideshow', array(
		'labels'            => array(
			'name'                => __( 'Slideshow'		  	  , 'bami' ),
			'singular_name'       => __( 'Slideshow'		  	  , 'bami' ),
			'all_items'           => __( 'Slideshow'  	 		  , 'bami' ),
			'new_item'            => __( 'New item'				  , 'bami' ),
			'add_new'             => __( 'Add New'				  , 'bami' ),
			'add_new_item'        => __( 'Add New item'			  , 'bami' ),
			'edit_item'           => __( 'Edit item'			  , 'bami' ),
			'view_item'           => __( 'View item'			  , 'bami' ),
			'search_items'        => __( 'Search items'			  , 'bami' ),
			'not_found'           => __( 'No item found'		  , 'bami' ),
			'not_found_in_trash'  => __( 'No item found in trash' , 'bami' ),
			'parent_item_colon'   => __( 'Parent item'			  , 'bami' ),
			'menu_name'           => __( 'Slideshow'	  	  	  , 'bami' ),

		),
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail'),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'			=> 'dashicons-format-gallery',
	) );

}
// init
add_action( 'init', 'slideshow_init', 20 );