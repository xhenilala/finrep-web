<?php 
/**
*  taxonomy_init
*/
function taxonomy_init() {
	$labels = array(
		'name'                       => _x( 'undefined'						, 'dutchhub' ),
		'singular_name'              => _x( 'undefined'						, 'dutchhub' ),
		'menu_name'                  => __( 'undefined'						, 'dutchhub' ),
		'all_items'                  => __( 'All items'						, 'dutchhub' ),
		'parent_item'                => __( 'Parent item'					, 'dutchhub' ),
		'parent_item_colon'          => __( 'Parent item:'				, 'dutchhub' ),
		'new_item_name'              => __( 'New item'					, 'dutchhub' ),
		'add_new_item'               => __( 'Add New item'				, 'dutchhub' ),
		'edit_item'                  => __( 'Edit item'					, 'dutchhub' ),
		'update_item'                => __( 'Update item'				, 'dutchhub' ),
		'view_item'                  => __( 'View item'					, 'dutchhub' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'dutchhub' ),
		'add_or_remove_items'        => __( 'Add or remove items'		, 'dutchhub' ),
		'choose_from_most_used'      => __( 'Choose from the most used'	, 'dutchhub' ),
		'popular_items'              => __( 'Popular items'				, 'dutchhub' ),
		'search_items'               => __( 'Search item'				, 'dutchhub' ),
		'not_found'                  => __( 'Not Found'					, 'dutchhub' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'plan_undefined', [ 'plan' ],  $args );
}

add_action( 'init', 'taxonomy_init', 40 );